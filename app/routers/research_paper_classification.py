import sys


sys.path.append("..")

from fastapi import APIRouter, UploadFile, Form, File
from fastapi.responses import HTMLResponse

from app.common.util.decorators import log
# from app.services.unpywall_email import UnpaywallEmailService
# from app.services.title_email import TitleEmailService
# from app.services.email import EmailService
from app.services.research_paper_classification import ClassificationService

router = APIRouter(
    prefix='/WHO_paper_classification',
    tags=['WHO_paper_classification']
)


@router.post('/research_paper_classification_using_title_UncataData', response_class=HTMLResponse, status_code=200)
@log(__name__)
def research_paper_classification_using_title_UncataData(title):
    classification_service = ClassificationService()
    return classification_service.Output_Uncata_Data(title)


@router.post('/research_paper_classification_using_title_WHOcataData', response_class=HTMLResponse, status_code=200)
@log(__name__)
def research_paper_classification_using_title_WHOcataData(title):
    classification_service = ClassificationService()
    return classification_service.Output_WHO_Cata_Data(title)

@router.post('/research_paper_classification_using_abstract_UncataData', response_class=HTMLResponse, status_code=200)
@log(__name__)
def research_paper_classification_using_abstract_UncataData(abstract):
    classification_service = ClassificationService()
    return classification_service.Output_Uncata_Data_Abstract(abstract)


@router.post('/research_paper_classification_using_abstract_WHOcataData', response_class=HTMLResponse, status_code=200)
@log(__name__)
def research_paper_classification_using_abstract_WHOcataData(abstract):
    classification_service = ClassificationService()
    return classification_service.Output_WHO_Cata_Data_Abstract(abstract)