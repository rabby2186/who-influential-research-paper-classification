import logging
import requests
import urllib.parse
import pickle
import json
import sys
import pandas as pd
import numpy as np
from app.services.Opencitation import Opencitation_,return_opencitation


class ClassificationService:

    def __init__(self):
        self.logger = logging.getLogger(__name__)
        super().__init__()

    def GetCrossrefAbstract(self, title):
        url_encoded_title = urllib.parse.quote_plus(title)
        url = 'https://api.crossref.org/works?rows=1&query.bibliographic={}'.format(url_encoded_title)

        response = requests.get(url)
        if not response.ok:
            self.logger.warning('Request error returns response: {}'.format(response.__dict__))
        response = response.json()
        if 'items' in response['message']:
            for item in response['message']['items']:
                #print(item)
                try:
                    abstract = item['abstract'].replace("<jats:title>Abstract</jats:title><jats:p>", "").replace("<jats:italic>","").replace("</jats:italic>",'').replace("</jats:p>","")
                    doi = item['DOI']
                    return abstract, doi
                    break
                except:
                    return "No abstract found in Crossref."

    def LoadingPickleMultiClass(self):
        vectorizer_multiclass = pickle.load(open(
            "../app/ML_model/label_prediction/WHO/rf_tf_idf_multiclass_experiments/vect_abstract_tfidf.pickle",
            'rb'))
        model_multiclass = pickle.load(open(
            "../app/ML_model/label_prediction/WHO/rf_tf_idf_multiclass_experiments/random_forest_tf_idf.pickle",
            'rb'))
        return vectorizer_multiclass, model_multiclass

    def LoadingPickleInfluentialLevel(self):
        vectorizer_influentiallevel = pickle.load(open(
            "../app/ML_model/influential_papers_experiments/influential_papers_experiments_uncategorized_data/rf_tfidf/vect_abstract_tfidf.pickle",
            'rb'))
        model_influentiallevel = pickle.load(open(
            "../app/ML_model/influential_papers_experiments/influential_papers_experiments_uncategorized_data/rf_tfidf/random_forest_tf_idf.pickle",
            'rb'))
        return vectorizer_influentiallevel, model_influentiallevel


    def LoadingPickleInfluentialLevelWHO(self):
        Long_Covid_vectorizer_influentiallevel = pickle.load(open(
            "../app/ML_model/influential_papers_experiments/influential_papers_experiments_WHO_categorized_data/rf_tfidf/vect_Long_Covid_df_with_cluster.pickle",
            'rb'))
        Long_Covid_model_influentiallevel = pickle.load(open(
            "../app/ML_model/influential_papers_experiments/influential_papers_experiments_WHO_categorized_data/rf_tfidf/Long_Covid_df_with_cluster.pickle",
            'rb'))
        Traditional_medicine_vectorizer_influentiallevel = pickle.load(open(
            "../app/ML_model/influential_papers_experiments/influential_papers_experiments_WHO_categorized_data/rf_tfidf/vect_Traditional_medicine_df_with_cluster.pickle",
            'rb'))
        Traditional_medicine_model_influentiallevel = pickle.load(open(
            "../app/ML_model/influential_papers_experiments/influential_papers_experiments_WHO_categorized_data/rf_tfidf/Traditional_medicine_df_with_cluster.pickle",
            'rb'))
        Vaccines_vectorizer_influentiallevel = pickle.load(open(
            "../app/ML_model/influential_papers_experiments/influential_papers_experiments_WHO_categorized_data/rf_tfidf/vect_Vaccines_df_with_cluster.pickle",
            'rb'))
        Vaccines_model_influentiallevel = pickle.load(open(
            "../app/ML_model/influential_papers_experiments/influential_papers_experiments_WHO_categorized_data/rf_tfidf/Vaccines_df_with_cluster.pickle",
            'rb'))
        Variants_vectorizer_influentiallevel = pickle.load(open(
            "../app/ML_model/influential_papers_experiments/influential_papers_experiments_WHO_categorized_data/rf_tfidf/vect_Variants_df_with_cluster.pickle",
            'rb'))
        Variants_model_influentiallevel = pickle.load(open(
            "../app/ML_model/influential_papers_experiments/influential_papers_experiments_WHO_categorized_data/rf_tfidf/Variants_df_with_cluster.pickle",
            'rb'))
        return Long_Covid_vectorizer_influentiallevel, Long_Covid_model_influentiallevel, Traditional_medicine_vectorizer_influentiallevel, Traditional_medicine_model_influentiallevel, Vaccines_vectorizer_influentiallevel, Vaccines_model_influentiallevel, Variants_vectorizer_influentiallevel, Variants_model_influentiallevel


    def LabelPredict(self, title):
        vectorizer_multiclass, model_multiclass = self.LoadingPickleMultiClass()
        try:
            abstract, doi = self.GetCrossrefAbstract(title)
            # print(doi)
            # print(abstract)
            pred = model_multiclass.predict(vectorizer_multiclass.transform([abstract]))[0]
            return pred
        except:
            return {"No abstract found in Crossref."}

    def InfluentialLevelPredictUncataData(self, title):
        vectorizer_influentiallevel, model_influentiallevel = self.LoadingPickleInfluentialLevel()
        try:
            abstract, doi = self.GetCrossrefAbstract(title)
            # print(doi)
            # print(abstract)
            pred = model_influentiallevel.predict(vectorizer_influentiallevel.transform([abstract]))[0]
            return pred
        except:
            return {"No abstract found in Crossref."}

    def InfluentialLevelPredictWHOcataData(self, title):
        Long_Covid_vectorizer_influentiallevel, Long_Covid_model_influentiallevel, Traditional_medicine_vectorizer_influentiallevel, Traditional_medicine_model_influentiallevel, Vaccines_vectorizer_influentiallevel, Vaccines_model_influentiallevel, Variants_vectorizer_influentiallevel, Variants_model_influentiallevel = self.LoadingPickleInfluentialLevelWHO()
        try:
            abstract, doi = self.GetCrossrefAbstract(title)
            pred_label = self.LabelPredict(title)
            if pred_label == "Long_Covid":
                pred = Long_Covid_model_influentiallevel.predict(Long_Covid_vectorizer_influentiallevel.transform([abstract]))[0]
                return pred
            if pred_label == "Traditional_medicine":
                pred = Traditional_medicine_model_influentiallevel.predict(Traditional_medicine_vectorizer_influentiallevel.transform([abstract]))[0]
                return pred
            if pred_label == "Vaccines":
                pred = Vaccines_model_influentiallevel.predict(Vaccines_vectorizer_influentiallevel.transform([abstract]))[0]
                return pred
            if pred_label == "Variants":
                pred = Variants_model_influentiallevel.predict(Variants_vectorizer_influentiallevel.transform([abstract]))[0]
                return pred
        except:
            return {"No abstract found in Crossref."}

    def CitationCount(self,title):
        try:
            abstract, doi = self.GetCrossrefAbstract(title)
            citation_count = return_opencitation(doi)
            return citation_count
        except:
            return {"No citation count found in Opencitation."}

    def Output_Uncata_Data(self,title):

        try:
            abstract, doi = self.GetCrossrefAbstract(title)
            result = {"DOI": doi, "Label": self.LabelPredict(title), "Influential Level": self.InfluentialLevelPredictUncataData(title)} #,"Citation count": self.CitationCount(title)
            return json.dumps(result)
        except:
            result = {"DOI": "No abstract found in Crossref.", "Label": "No abstract found in Crossref.","Influential Level": "No abstract found in Crossref."}
            return json.dumps(result)

    def Output_WHO_Cata_Data(self,title):
        #abstract, doi = self.GetCrossrefAbstract(title)
        try:
            abstract, doi = self.GetCrossrefAbstract(title)
            result = {"DOI": doi, "Label": self.LabelPredict(title), "Influential Level": self.InfluentialLevelPredictWHOcataData(title)} #, "Citation count": self.CitationCount(title)
            return json.dumps(result)
        except:
            result = {"DOI": "No abstract found in Crossref.", "Label": "No abstract found in Crossref.","Influential Level": "No abstract found in Crossref."}
            return json.dumps(result)


# with abstract

    def LabelPredictAbstract(self, abstract):
        vectorizer_multiclass, model_multiclass = self.LoadingPickleMultiClass()
        try:
            pred = model_multiclass.predict(vectorizer_multiclass.transform([abstract]))[0]
            return pred
        except:
            return {"No abstract found in Crossref."}

    def InfluentialLevelPredictUncataDataAbstract(self, abstract):
        vectorizer_influentiallevel, model_influentiallevel = self.LoadingPickleInfluentialLevel()
        try:
            pred = model_influentiallevel.predict(vectorizer_influentiallevel.transform([abstract]))[0]
            return pred
        except:
            return {"No abstract found in Crossref."}

    def InfluentialLevelPredictWHOcataDataAbstract(self, abstract):
        Long_Covid_vectorizer_influentiallevel, Long_Covid_model_influentiallevel, Traditional_medicine_vectorizer_influentiallevel, Traditional_medicine_model_influentiallevel, Vaccines_vectorizer_influentiallevel, Vaccines_model_influentiallevel, Variants_vectorizer_influentiallevel, Variants_model_influentiallevel = self.LoadingPickleInfluentialLevelWHO()
        try:
            pred_label = self.LabelPredictAbstract(abstract)
            if pred_label == "Long_Covid":
                pred = Long_Covid_model_influentiallevel.predict(Long_Covid_vectorizer_influentiallevel.transform([abstract]))[0]
                return pred
            if pred_label == "Traditional_medicine":
                pred = Traditional_medicine_model_influentiallevel.predict(Traditional_medicine_vectorizer_influentiallevel.transform([abstract]))[0]
                return pred
            if pred_label == "Vaccines":
                pred = Vaccines_model_influentiallevel.predict(Vaccines_vectorizer_influentiallevel.transform([abstract]))[0]
                return pred
            if pred_label == "Variants":
                pred = Variants_model_influentiallevel.predict(Variants_vectorizer_influentiallevel.transform([abstract]))[0]
                return pred
        except:
            return {"No abstract found in Crossref."}

    def Output_Uncata_Data_Abstract(self,abstract):
        result = {"Label": self.LabelPredictAbstract(abstract), "Influential Level": self.InfluentialLevelPredictUncataDataAbstract(abstract)} #, "Citation count": self.CitationCount(abstract)
        return json.dumps(result)

    def Output_WHO_Cata_Data_Abstract(self,abstract):
        result = {"Label": self.LabelPredictAbstract(abstract), "Influential Level": self.InfluentialLevelPredictWHOcataDataAbstract(abstract)} #, "Citation count": self.CitationCount(abstract)
        return json.dumps(result)