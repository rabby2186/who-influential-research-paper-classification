import opencitingpy


class Opencitation_:

    def __init__(self, author: list, citation: list, citation_count: int, data: dict, doi: str, issue: int,
                 oa_link: str, page: str, source_id: str, source_title: str, title: str, volume: int,
                 year: int) -> None:
        self.parse(author, citation, citation_count, data, doi, issue, oa_link, page, source_id, source_title,
                   title, volume, year)

        pass

    def parse(self, author: list, citation: list, citation_count: int, data: dict, doi: str, issue: int,
              oa_link: str, page: str, source_id: str, source_title: str, title: str, volume: int, year: int):
        self.author = author
        self.citation = citation
        self.citation_count = citation_count
        self.data = data
        self.doi = doi
        self.issue = issue
        self.oa_link = oa_link
        self.page = page
        self.source_id = source_id
        self.source_title = source_title
        self.title = title
        self.volume = volume
        self.year = year
        pass

    def get_author(self) -> list:
        return self.author

    def get_citation(self) -> list:
        return self.citation

    def get_citation_count(self) -> int:
        return self.citation_count

    def get_doi(self) -> str:
        return self.doi

    def get_issue(self) -> int:
        return self.issue

    def get_oa_link(self) -> str:
        return self.oa_link

    def get_page(self) -> str:
        return self.page

    def get_source_id(self) -> str:
        return self.source_id

    def get_source_title(self) -> str:
        return self.source_title

    def get_title(self) -> str:
        return self.title

    def get_volume(self) -> int:
        return self.volume

    def get_year(self) -> int:
        return self.year

client = opencitingpy.client.Client()

    # dois = ['10.1007/978-3-030-61244-3_6']

def return_opencitation(dois):
        # get metadata of a list of articles, including title, publication year, number of citing and cited documents, etc.
    metadata = client.get_metadata(dois)

    opencitation = Opencitation_(metadata[0].author, metadata[0].citation, metadata[0].citation_count,
                                    metadata[0].data, metadata[0].doi, metadata[0].issue,
                                    metadata[0].oa_link, metadata[0].page, metadata[0].source_id,
                                    metadata[0].source_title, metadata[0].title, metadata[0].volume, metadata[0].year)

    return opencitation.get_citation_count()








