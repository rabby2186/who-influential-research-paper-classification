# WHO research paper classification API

[![pipeline status](https://gitlab.com/TIBHannover/orkg/nlp/orkg-nlp-api/badges/main/pipeline.svg)](https://gitlab.com/TIBHannover/orkg/nlp/orkg-nlp-api/-/commits/main)

REST API for the WHO research paper classification python.

This API provides the WHO COVID-19 related scholarly documents label and influential level 
from ``TITLE`` and ``ABSTRACT``.

## Prerequisites

We require a python version `3.7` or above.

Requirement by service:

| Service                    | Requirement(s)    |
|----------------------------|-------------------|
| `WHO_paper_classification/research_paper_classification_using_title_UncataData`          | `Title: provide title of a COVID-19 related research paper`|
| `WHO_paper_classification/research_paper_classification_using_title_WHOcataData`          | `Title: provide title of a COVID-19 related research paper`|
| `WHO_paper_classification/research_paper_classification_using_abstract_UncataData`          | `abstract: provide abstract of a COVID-19 related research paper`|
| `WHO_paper_classification/research_paper_classification_using_abstract_WHOcataData`          | `abstract: provide abstract of a COVID-19 related research paper`|


## Output Demo  (Example)
For using title (`WHO_paper_classification/research_paper_classification_using_title_UncataData`)

`Title: COVID-19 pathophysiology: A review`
```commandline
{"DOI": "10.21203/rs.3.rs-142174/v1", "Label": "Variants", "Influential Level": "high"}
```

For using title (`WHO_paper_classification/research_paper_classification_using_title_WHOcataData`)

`Title: COVID-19 pathophysiology: A review`
```commandline
{"DOI": "10.21203/rs.3.rs-142174/v1", "Label": "Variants", "Influential Level": "low"}
```

For using abstract (`WHO_paper_classification/research_paper_classification_using_abstract_UncataData`)

`Abstract: In December 2019, a novel coronavirus, now named as SARS-CoV-2, caused a series of acute atypical respiratory diseases in Wuhan, Hubei Province, China. The disease caused by this virus was termed COVID-19. The virus is transmittable between humans and has caused pandemic worldwide. The number of death tolls continues to rise and a large number of countries have been forced to do social distancing and lockdown. Lack of targeted therapy continues to be a problem. Epidemiological studies showed that elder patients were more susceptible to severe diseases, while children tend to have milder symptoms. Here we reviewed the current knowledge about this disease and considered the potential explanation of the different symptomatology between children and adults.`
```commandline
{"Label": "Long_Covid", "Influential Level": "low"}
```

For using abstract (`WHO_paper_classification/research_paper_classification_using_abstract_WHOcataData`)

`Abstract: In December 2019, a novel coronavirus, now named as SARS-CoV-2, caused a series of acute atypical respiratory diseases in Wuhan, Hubei Province, China. The disease caused by this virus was termed COVID-19. The virus is transmittable between humans and has caused pandemic worldwide. The number of death tolls continues to rise and a large number of countries have been forced to do social distancing and lockdown. Lack of targeted therapy continues to be a problem. Epidemiological studies showed that elder patients were more susceptible to severe diseases, while children tend to have milder symptoms. Here we reviewed the current knowledge about this disease and considered the potential explanation of the different symptomatology between children and adults.`
```commandline
{"Label": "Long_Covid", "Influential Level": "low"}
```


## Run in CMD (Example)

```commandline
curl -X 'POST' \
'http://127.0.0.1:8000/WHO_paper_classification/research_paper_classification_using_title_UncataData?title=COVID-19%20pathophysiology%3A%20A%20review' \
-H 'accept: text/html' \
-d ''


```

## How to run

### With ``docker-compose``


```commandline

```

### Manually
```commandline
Run FastAPI:

uvicorn main:app --reload

```
For local development you may run the web server using ``uvicorn`` with the ``--reload`` option:

```commandline
uvicorn app.main:app --host 0.0.0.0 --port 4321 --reload
```


## API Documentation
After successfully running the application, check the documentation at `localhost:4321/docs`
or `localhost:4321/redoc` (please adapt your `host:port` in case you configured them).
